from flask import Flask, render_template, request
#render_template взаимодействует с html-страницами

import pickle
import sklearn
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import AdaBoostRegressor

app = Flask(__name__)

@app.route('/', methods=['get', 'post']) #http://127.0.0.1:5000 + '/' = http://127.0.0.1:5000/
def main():
    y_pred = ''
    y1_pred = ''
    if request.method == "POST":
        IW = float(request.form.get("IW"))
        IF = float(request.form.get("IF"))
        VW = float(request.form.get("VW"))
        FP = float(request.form.get("FP"))

        itog = ([[IW, IF, VW, FP]])
        scaler = pickle.load(open('scaler.sav', 'rb'))
        itog_t = scaler.transform(itog)

        with open('rf_model.pkl', 'rb') as f:
            loaded_model = pickle.load(f)
        y_pred = loaded_model.predict(itog_t)
        scaler_y = pickle.load(open('scaler_y.sav', 'rb'))
        y_pred = scaler_y.inverse_transform(y_pred.reshape(-1, 1))

        with open('ada_model.pkl', 'rb') as f1:
            loaded_model = pickle.load(f1)
        y1_pred = loaded_model.predict(itog_t)
        scaler_y1 = pickle.load(open('scaler_y1.sav', 'rb'))
        y1_pred = scaler_y1.inverse_transform(y1_pred.reshape(-1, 1))

    return render_template('index.html', result=y_pred,  result1=y1_pred)
